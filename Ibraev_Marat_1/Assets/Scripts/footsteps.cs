﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class footsteps : MonoBehaviour
{
    AudioSource _audiosource;
    public AudioClip[] runnigclips; 
    public AudioClip[] walkingclips; 
    vThirdPersonInput tpInput; // Переменная для  обращения к контроллеру персонажа и для того, чтобы проверять магнитуду. Таким образом шаги не будут звучать, когда мы стоим на месте
    vThirdPersonController tpController; // Переменная для обращения к компоненту контроллера. Для проверки стейта спринта
    public float VolumeMin = 1f; // Минимальная громкость шагов для рандома громкости
    public float pitchMin = 1f; // Минимальное значение питча для рандома
    public float pitchMax = 1f; // Максимальное значение питча для рандома
    int lastIndex; // переменная для проверки какой был прошлый индекс сэмпла, чтобы не повторялся один и тот же сэмпл
    int newIndex; // переменная для запоминания нового сформированного индекса сэмпла

    void Start()
    {
        _audiosource = GetComponent<AudioSource>(); // Говорим, что переменной audiosource присваевается путь к компоненту аудиосорс, который висит на компоненте (вешаем скрипт на игрока)
        tpInput = GetComponent<vThirdPersonInput>(); // обращаемся к компоненту инпута
        tpController = GetComponent<vThirdPersonController>(); // обращаемся к контроллеру
    }


    void footstep() // функция проигрывания шагов, которая вызывается из анимации
    {
        if (tpInput.cc.inputMagnitude > 0.1) // если значение магнитуды больше 0.1 - значит мы двигаемся
        {
            _audiosource.volume = Random.Range(VolumeMin, 1f); // выставляем случайное значние громкости от минимума для максимум 1f 
            _audiosource.pitch = Random.Range(pitchMin, pitchMax); // выставляем случайный питч

            if (tpController.isSprinting) // проверяем если нас персонаж бежит
            {

                Randomization(runnigclips.Length); // запускаем функцию рандомизации номера клипа Randomization
                _audiosource.PlayOneShot(runnigclips[newIndex]); // воспроизводим сэмпл случайного индекса
                lastIndex = newIndex; // записываем индекс, который проиграли, для того чтобы не повторить его снова
            }

            else // в противном случае = он идет
            {
                Randomization(walkingclips.Length); // запускаем функцию рандомизации номера клипа Randomization
                _audiosource.PlayOneShot(walkingclips[newIndex]); // воспроизводим сэмпл случайного индекса
                lastIndex = newIndex; // записываем индекс, который проиграли, для того чтобы не повторить его снова
            }
        }

    }


    void Randomization(int cliplenght) // созданная нами функция для выбора случайного значения индекса сэмпла. в эту функицию мы передаем число сколько всего у нас сэмплов в массиве
    {
        newIndex = Random.Range(0, cliplenght); // присваеваем переменной newIndex случайное число от 0 до числа переданноего в функцию (а мы передаем в нее количество сэмплов в массиве)
        while (newIndex == lastIndex) // запускаем цикл, если новый индекс будет равен такому же числу как уже было = если сэмпл будет такой же, то
            newIndex = Random.Range(0, cliplenght); // снова запускаем рандом, пока не получим новое число для индекса
    }

}
