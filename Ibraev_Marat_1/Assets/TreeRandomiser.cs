﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeRandomiser : MonoBehaviour
{
    public AudioClip Tree_clip;
    public AudioSource Tree_source;
    void Start()
    {
        Tree_source = GetComponent<AudioSource>();
        Tree_source.PlayDelayed(Random.Range(0.2f, 1f));
        Tree_source.loop = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
